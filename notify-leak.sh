#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# dfncred
# Parse DFN credential notifications.
# Create notification mails for users
# Automatically notify users using `signmail`
###

# REQUIRED: path to signmail executable
SIGNMAIL="smail"

# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# usage
usage() {
  echo "$(basename "$0") parse DFN/haveibeenpwned style credential notes and notify users."
  echo "usage: $(basename "$0") -s [subject] -l [leak source] -d [delimiter] -c <signer_certificate> -m <mail_infile> -f </path/to/data.csv>";
  echo ""
  echo -e "\t-f </path/to/data.csv>:  file with leak information. format: user'delim'passes"
  echo -e "\t-m <mail_infile>:        file with the email to sign"
  echo -e "\t-c <signer_certificate>: smime certificate used to sign the email"
  echo -e "\t-s [subject]:            email subject. Default: 'Data Leak Notification'"
  echo -e "\t-l [leak source]:        source of the leak. Default: 'haveibeenpwned'"
  echo -e "\t-d [delimiter]:          delimiter for reading data file. Default: '|' (DFN style)"
  exit 0;
}

# check required commands
command -v openssl >/dev/null 2>&1 || { echo >&2 "ERROR: openssl required."; usage; exit 1; }
command -v sendmail >/dev/null 2>&1 || { echo >&2 "ERROR: sendmail required."; usage; exit 1; }
command -v "${SIGNMAIL}" >/dev/null 2>&1 || { echo >&2 "ERROR: signmail command not found. Required."; usage; exit 1; }

# arguments
# parsing delimiter, set to DFN default
IFS='|'

# source of the leak
LEAKSOURCE=""

# path to the mail to send
MAIL_LEAK=""

# subject of the mail
SUBJECT=""

# mail signer certificate (not required but very strongly recommended)
SIGNER=""

# config
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
while getopts ":hd:l:m:f:s:c:" OPT; do
  case "${OPT}" in
    d)
      IFS="$(echo -e "${OPTARG}" | tr -d '[:space:]')";
      ;;
    c)
      SIGNER="${OPTARG}";
      ;;
    m)
      MAIL_LEAK="${OPTARG}";
      ;;
    s)
      SUBJECT="${OPTARG}";
      ;;
    l)
      LEAKSOURCE="${OPTARG}";
      ;;
    h)
      usage;
      exit 1;
      ;;
    f)
      DATA="${OPTARG}";
      ;;
    *)
      usage;
      exit 1;
      ;;
  esac
done
shift $((OPTIND-1))

# check arguments, set defaults if appropriate
if [ -z "${MAIL_LEAK}" ] || [ ! -f "${MAIL_LEAK}" ]; then
  echo >&2 "ERROR: no mail template found.";
  usage; exit 1
fi

if [ -z "${SIGNER}" ]; then
  echo >&2 "ERROR: no signer certificate found. Required.";
  usage; exit 1
fi

if [ -z "${SUBJECT}" ]; then
  echo >&2 "WARN: subject empty, using default.";
  SUBJECT="Data Leak Notification";
fi

if [ -z "${LEAKSOURCE}" ]; then
  echo >&2 "WARN: leaksource empty, using default.";
  LEAKSOURCE="haveibeenpwned"
fi

if [ ! -f "${DATA}" ]; then
  echo >&2 "ERROR: data file not found";
  usage; exit 1;
fi

echo "processing ${DATA}"
#while IFS='|' read -r user pass; do
while read -r user pass; do
  #LEAKINFO="$user|$pass" envsubst < "${MAIL}" > "${user}".txt;
  export LEAKINFO="$user|$pass" LEAKSOURCE="$LEAKSOURCE"
  envsubst '${LEAKINFO} ${LEAKSOURCE}' < "${MAIL_LEAK}" > "${user}".txt;
  "${SIGNMAIL}" -c "${SIGNER}" -t "${user}" -s "${SUBJECT}" -m "${user}".txt;
  rm "${user}".txt;
done < "${DATA}"
