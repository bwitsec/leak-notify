#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# - parse list of abuse notifications.
# - find the contact responsible for the abused system
# - send a abuse notification email to responsible contact
###

# REQUIRED: path to signmail executable
SIGNMAIL="smail"
FCONTACT="find-contact.py"

# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# usage
usage() {
  echo "$(basename "$0") parse DFN/haveibeenpwned style credential notes and notify users."
  echo "usage: $(basename "$0") -s [subject] -l [leak source] -d [delimiter] -c <signer_certificate> -m <mail_infile> -f </path/to/data.csv>";
  echo ""
  echo -e "\t-f </path/to/data.csv>:  file with abuse information. format: ts,ip,tag,count,messages"
  echo -e "\t-m </path/to/mail_infile>:        file with the email to sign"
  echo -e "\t-c <signer_certificate>: smime certificate used to sign the email"
  echo -e "\t-i </path/to/contacts>:  file with contact information (ips to emails)"
  echo -e "\t-s [subject]:            email subject. Default: 'Abuse Notification'"
  echo -e "\t-d [delimiter]:          delimiter for reading data file. Default: ',' (csv)"
  exit 0;
}

# check required commands
command -v openssl >/dev/null 2>&1 || { echo >&2 "ERROR: openssl required."; usage; exit 1; }
command -v sendmail >/dev/null 2>&1 || { echo >&2 "ERROR: sendmail required."; usage; exit 1; }
command -v "${SIGNMAIL}" >/dev/null 2>&1 || { echo >&2 "ERROR: signmail command not found. Required."; usage; exit 1; }
command -v "${FCONTACT}" >/dev/null 2>&1 || { echo >&2 "ERROR: find-contact command not found. Required."; usage; exit 1; }

# arguments
# parsing delimiterd
IFS=','

# path to the mail to send
MAIL_ABUSE=""

# subject of the mail
SUBJECT="Abuse Notification"

# data file
DATA=""

# mail signer certificate 
SIGNER=""

# contact csv 
CONTACTS=""

# config
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
while getopts ":hd:i:m:f:s:c:" OPT; do
  case "${OPT}" in
    d)
      IFS="$(echo -e "${OPTARG}" | tr -d '[:space:]')";
      ;;
    c)
      SIGNER="${OPTARG}";
      ;;
    m)
      MAIL_ABUSE="${OPTARG}";
      ;;
    s)
      SUBJECT="${OPTARG}";
      ;;
    i)
      CONTACTS="${OPTARG}";
      ;;
    h)
      usage;
      exit 1;
      ;;
    f)
      DATA="${OPTARG}";
      ;;
    *)
      usage;
      exit 1;
      ;;
  esac
done
shift $((OPTIND-1))

# check arguments, set defaults if appropriate
if [ -z "${MAIL_ABUSE}" ] || [ ! -f "${MAIL_ABUSE}" ]; then
  echo >&2 "ERROR: no mail template found.";
  usage; exit 1
fi

if [ -z "${SIGNER}" ]; then
  echo >&2 "ERROR: no signer certificate found. Required.";
  usage; exit 1
fi

if [ -z "${SUBJECT}" ]; then
  echo >&2 "WARN: subject empty, using default.";
  SUBJECT="Abuse Notification"
fi

if [ ! -f "${CONTACTS}" ]; then
  echo >&2 "ERROR: contacts file not found";
  usage; exit 1;
fi

if [ ! -f "${DATA}" ]; then
  echo >&2 "ERROR: data file not found";
  usage; exit 1;
fi

echo "processing ${DATA}"
#while IFS='|' read -r user pass; do
while IFS=',' read -r ts ip tag count raw; do
  export IP=$ip
  export TS=$ts
  export TAG=$tag
  export RAW=$raw

  email=$("$FCONTACT" -i "$ip" -c "$CONTACTS" -f email0 -n)
  email1=$("$FCONTACT" -i "$ip" -c "$CONTACTS" -f email1 -n)
  email="${email// /}"
  email1="${email1// /}"

  if [ -z $email ]; then
      echo >&2 "WARN: no contact found for IP $ip";
      echo "${ts},${ip},${tag},${count},${raw}" >> abuses-not-processed.csv;
      continue
  fi

  echo "send to email0: $email"
  mailname="${ts}-${ip}-${email}.txt"
  envsubst '${TS} ${IP} ${TAG} ${RAW}' < "${MAIL_ABUSE}" > "${mailname}";
  "${SIGNMAIL}" -c "${SIGNER}" -t "${email}" -s "${SUBJECT}" -m "${mailname}";
  rm "${mailname}";

  if [ ! -z $email1 ]; then
      echo "send to email1: $email1"
      mailname="${ts}-${ip}-${email1}.txt"
      envsubst '${TS} ${IP} ${TAG} ${RAW}' < "${MAIL_ABUSE}" > "${mailname}";
      "${SIGNMAIL}" -c "${SIGNER}" -t "${email1}" -s "${SUBJECT}" -m "${mailname}";
      rm "${mailname}";
  fi

  echo "${ts},${ip},${tag},${count},${raw}" >> abuses-processed.csv;
done < "${DATA}"

