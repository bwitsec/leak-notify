#!/usr/bin/env python3
###
# Copyright (c) 2019 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# leak_remove_duplicates
# Parse DFN or haveibeenpwned credential notifications and remove duplicate usernames (email addresses) and passwords.
# If a user has multiple passwords they are added to a set.
###
import csv
import os
import sys
import argparse


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", required=True, help="path to credential file")
    parser.add_argument("-d", "--delimiter", required=False, help="delimiter in credential file", default='|')
    arg_vars = vars(parser.parse_args())
    return arg_vars
    
    
def remove_duplicates(arg_vars):
    with open(arg_vars["file"], 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=arg_vars["delimiter"])
            uniqued = {}
            for row in reader:
                    user = row[0].lower()
                    passwd = row[1].rstrip('_') if len(row)==2 else ""
                    try:
                            uniqued[user].add(passwd)
                    except KeyError:
                            uniqued[user] = set([passwd])
    return uniqued


def write_credentials(filename, uniqued):
    basename = os.path.basename(filename)
    f_name, f_ext = os.path.splitext(basename)
    outfile = f_name + '_noduplicates' + f_ext
    handle = open(outfile,'w')
    for k,v in uniqued.items():
        #print('%s|%s' %(k,str(v)[1:-1]))
        #print('%s|%s' %(k,str(v)[1:-1]),file=handle)
        for pw in v:
            print('%s|%s' %(k,str(pw)))
            print('%s|%s' %(k,str(pw)),file=handle)
    handle.close()


if __name__ == '__main__':
    arg_vars = parse_arguments()
    removed_duplicates = remove_duplicates(arg_vars)
    write_credentials(arg_vars["file"], removed_duplicates)
