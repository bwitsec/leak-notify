# dfncred

* Parse DFN credential notifications (delimiter '|').
* Parse haveibeenpwned credential notifications (delimiter ':').
* Create notification mails for users
* Automatically notify users using `signmail`

This script parses credential notification from DFN-Cert or haveibeenpwned, generates personalized emails and notifies affected users with a signed mail using `signmail`.

The leak data must have the following format:

```
username${IFS}password
```

where ${IFS} is the appropriate delimiter. At the time of writing this is '|' for DFN-cert and ':' for haveibeenpwned.

## Requirements

To be able to send the mail, the script requires `signmail` which requires `openssl` and `sendmail` with a configured mail transfer agent (MTA) like e.g. `msmtp`, `opensmtpd`, `postfix`, `exim4`.

* `signmail`
* `openssl`
* `sendmail` along with any configured MTA

## Usage:

```
leak_notify.sh parse DFN/haveibeenpwned style credential notes and notify users.
usage: leak_notify.sh -s [subject] -l [leak source] -d [delimiter] -c <signer_certificate> -m <mail_infile> -f </path/to/data.csv>

	-f </path/to/data.csv>:  file with leak information. format: user'delim'passes
	-m <mail_infile>:        file with the email to sign
	-c <signer_certificate>: smime certificate used to sign the email
	-s [subject]:            email subject. Default: 'Data Leak Notification'
	-l [leak source]:        source of the leak. Default: 'haveibeenpwned'
	-d [delimiter]:          delimiter for reading data file. Default: '|' (DFN style)
```

E.g.:

```
./leak_notify.sh -s "password leak" -d ":" -c signercert.pem -m example_mail.txt -f example_data.csv
```

The mail template must set the content type header in the first line:

```
Content-Type: text/plain; charset=UTF-8
```