#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# Notify users of accessible printers
# Create notification mails for users
# Automatically notify users using `signmail`
###

# REQUIRED: path to signmail and find-contacts executable
SIGNMAIL="smail"
FCONTACT="find-contact.py"

# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# usage
usage() {
  echo "$(basename "$0") parse file with printer information and notify users.."
  echo "usage: $(basename "$0") -s [subject] -l [leak source] -d [delimiter] -c <signer_certificate> -m <mail_infile> -f </path/to/data.csv>";
  echo ""
  echo -e "\t-f </path/to/data.csv>:  file with leak information. format: ip'delim'port'delim'host"
  echo -e "\t-m <mail_infile>:        file with the email to sign"
  echo -e "\t-c <signer_certificate>: smime certificate used to sign the email"
  echo -e "\t-s [subject]:            email subject. Default: 'Accessible Printer'"
  echo -e "\t-d [delimiter]:          delimiter for reading data file. Default: '|' (DFN style)"
  exit 0;
}

# check required commands
command -v openssl >/dev/null 2>&1 || { echo >&2 "ERROR: openssl required."; usage; exit 1; }
command -v sendmail >/dev/null 2>&1 || { echo >&2 "ERROR: sendmail required."; usage; exit 1; }
command -v "${SIGNMAIL}" >/dev/null 2>&1 || { echo >&2 "ERROR: signmail command not found. Required."; usage; exit 1; }
command -v "${FCONTACT}" >/dev/null 2>&1 || { echo >&2 "ERROR: find-contact command not found. Required."; usage; exit 1; }

# arguments
# parsing delimiter, set to DFN default
IFS='|'

# source of the leak
LEAKSOURCE=""

# path to the mail to send
MAIL_PRINTER=""

# subject of the mail
SUBJECT="Accessible Printer"

# mail signer certificate (not required but very strongly recommended)
SIGNER=""

# data file
DATA=""

# contact csv 
CONTACTS=""

# config
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
while getopts ":hd:m:f:s:c:" OPT; do
  case "${OPT}" in
    d)
      IFS="$(echo -e "${OPTARG}" | tr -d '[:space:]')";
      ;;
    c)
      SIGNER="${OPTARG}";
      ;;
    m)
      MAIL_PRINTER="${OPTARG}";
      ;;
    s)
      SUBJECT="${OPTARG}";
      ;;
    h)
      usage;
      exit 1;
      ;;
    f)
      DATA="${OPTARG}";
      ;;
    *)
      usage;
      exit 1;
      ;;
  esac
done
shift $((OPTIND-1))

# check arguments, set defaults if appropriate
if [ -z "${MAIL_PRINTER}" ] || [ ! -f "${MAIL_PRINTER}" ]; then
  echo >&2 "ERROR: no mail template found.";
  usage; exit 1
fi

if [ -z "${SIGNER}" ]; then
  echo >&2 "ERROR: no signer certificate found. Required.";
  usage; exit 1
fi

if [ -z "${SUBJECT}" ]; then
  echo >&2 "WARN: subject empty, using default.";
  SUBJECT="Accessible Printer";
fi

if [ ! -f "${DATA}" ]; then
  echo >&2 "ERROR: data file not found";
  usage; exit 1;
fi

echo "processing ${DATA}"
#while read -r ip port host; do
while read -r ip mac if room; do
  export IP="$ip"
  #export PORT="$port"
  #export HOST="$host"
  export IF="$if"
  export ROOM="$room"

  email=$("$FCONTACT" -i "$ip" -c "$CONTACTS" -f email0 -n)
  email1=$("$FCONTACT" -i "$ip" -c "$CONTACTS" -f email1 -n)
  email="${email// /}"
  email1="${email1// /}"
 
  if [ -z $email ]; then
      #echo >&2 "WARN: no contact for: $ip$IFS$port$IFS$host";
      #echo "$ip$IFS$port$IFS$host" >> printer-notification-error.csv ;
      echo >&2 "WARN: no contact for: $ip$IFS$mac$IFS$if$IFS$room";
      echo "$ip$mac$IFS$room" >> printer-notification-error.csv ;
      continue
  fi

  echo "send to $email"
  #mailname="${ip}-${port}-${email}.txt"
  #envsubst '${IP} ${PORT} ${HOST}' < "${MAIL_PRINTER}" > "${mailname}";
  mailname="${ip}-${email}.txt"
  envsubst '${IP} ${IF} ${ROOM}' < "${MAIL_PRINTER}" > "${mailname}";
  "${SIGNMAIL}" -c "${SIGNER}" -t "${email}" -s "${SUBJECT}" -m "${mailname}";
  rm "${mailname}";

  if [ ! -z $email1 ]; then
      echo "send to $email1"
      #mailname="${ip}-${port}-${email1}.txt"
      #envsubst '${IP} ${PORT} ${HOST}' < "${MAIL_PRINTER}" > "${mailname}";
      mailname="${ip}-${email}.txt"
      envsubst '${IP} ${IF} ${ROOM}' < "${MAIL_PRINTER}" > "${mailname}";
      "${SIGNMAIL}" -c "${SIGNER}" -t "${email}" -s "${SUBJECT}" -m "${mailname}";
      rm "${mailname}";
  fi
done < "${DATA}"
