#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# dfncred
# Parse DFN credential notifications.
# Create notification mails for users
# Automatically notify users using `signmail`
###

# REQUIRED: path to signmail executable
SIGNMAIL="smail"

# REQUIRED: path to haveibeenpwned breaches executable
PWNED="breaches.sh"


# error handling
set -eu -o pipefail
shopt -s failglob inherit_errexit

# usage
usage() {
  echo "$(basename "$0") parse DFN/haveibeenpwned style credential notes and notify users."
  echo "usage: $(basename "$0") -s [subject] -l [leak source] -d [delimiter] -c <signer_certificate> -m <mail_infile> -f </path/to/data.csv>";
  echo ""
  echo -e "\t-f </path/to/data.csv>:  file with leak information. format: user'delim'passes"
  echo -e "\t-m <mail_infile>:        file with the email to sign"
  echo -e "\t-c <signer_certificate>: smime certificate used to sign the email"
  exit 0;
}

# check required commands
command -v openssl >/dev/null 2>&1 || { echo >&2 "ERROR: openssl required."; usage; exit 1; }
command -v sendmail >/dev/null 2>&1 || { echo >&2 "ERROR: sendmail required."; usage; exit 1; }
command -v "${SIGNMAIL}" >/dev/null 2>&1 || { echo >&2 "ERROR: signmail command not found. Required."; usage; exit 1; }
command -v "${PWNED}" >/dev/null 2>&1 || { echo >&2 "ERROR: breaches command not found. Required."; usage; exit 1; }

# arguments
IFS=','

# haveibeenpwned info
BREACH=""
DATACLASSES=""
LINK=""

# path to the mail to send
MAIL_PWNED=""

# subject of the mail
SUBJECT=""

# mail signer certificate (not required but very strongly recommended)
SIGNER=""

INFILE=""

# config
config_home="${XDG_CONFIG_HOME:-$HOME/.config}/cert/"
config="${config_home}/cert.conf"
[[ -f "$config" ]] && . "$config"

# check arguments
while getopts ":hd::m:f:c:" OPT; do
  case "${OPT}" in
    d)
      IFS="$(echo -e "${OPTARG}" | tr -d '[:space:]')";
      ;;
    c)
      SIGNER="${OPTARG}";
      ;;
    m)
      MAIL_PWNED="${OPTARG}";
      ;;
    h)
      usage;
      exit 1;
      ;;
    f)
      INFILE="${OPTARG}";
      ;;
    *)
      usage;
      exit 1;
      ;;
  esac
done
shift $((OPTIND-1))

# check arguments, set defaults if appropriate
if [ -z "${MAIL_PWNED}" ] || [ ! -f "${MAIL_PWNED}" ]; then
  echo >&2 "ERROR: no mail template found.";
  usage; exit 1
fi

if [ -z "${SIGNER}" ]; then
  echo >&2 "ERROR: no signer certificate found. Required.";
  usage; exit 1
fi

if [ ! -f "${INFILE}" ]; then
  echo >&2 "INFO: data file not found, reading from stdin";
  INFILE=/dev/stdin
  #usage; exit 1;
fi

echo "processing ${INFILE}"
#while IFS='|' read -r user pass; do
while read -r date breach user; do
  export BREACH="$breach"
  export LINK="https://haveibeenpwned.com/PwnedWebsites#${breach}"
  export SUBJECT="Dataleak ${breach}"
  export DATACLASSES=$($PWNED $breach | jq ".DataClasses")
  envsubst '${BREACH} ${DATACLASSES} ${LINK}' < "${MAIL_PWNED}" > "${user}".txt;
  "${SIGNMAIL}" -c "${SIGNER}" -t "${user}" -s "${SUBJECT}" -m "${user}".txt;
  rm "${user}".txt;
done < "${INFILE:-/dev/stdin}"
